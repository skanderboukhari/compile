from django.shortcuts import render
from django.views import generic

from .runcode import *

default_c_code = """#include <stdio.h>

int main(int argc, char **argv)
{
    printf("Hello C World!!\\n");
    return 0;
}    
"""

default_cpp_code = """#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
    cout << "Hello C++ World" << endl;
    return 0;
}
"""

default_py_code = """import sys
import os

if __name__ == "__main__":
    print ("Hello Python World!!")
"""


def run(request):
    return render(request,'runcode/main.html')



def get(self,request):

    form = self.form_class(None)
    return render(request,'runcode/base.html',{'form':form})

def runpy(request):

    if request.method == 'POST':

        code = request.POST['code']
        run = RunPyCode(code)
        rescompil, resrun = run.run_py_code()
        resrun=resrun
        rescomp=rescompil

        if not resrun:
            resrun = 'No result!'

    else:
        code = default_py_code
        resrun = 'No result!'
        rescompil = "No compilation for Python"

    return render (request,'runcode/main.html', {'code':code,'resrun':resrun,'rescomp':rescompil})

def runc(request):
    if request.method == 'POST':
        code = request.POST['code']
        run = RunCCode(code)
        rescompil, resrun = run.run_c_code()
        resrun = resrun
        rescomp = rescompil

        if not resrun:
            resrun = 'No result!'
    else:
        code = default_c_code
        resrun = 'No result!'
        rescompil = 'No compilation for C+'
    return render (request,'runcode/main.html', {'code':code,'resrun':resrun,'rescomp':rescompil})


def runcpp(request):
    if request.method == 'POST':
        code = request.POST['code']
        run = RunCppCode(code)
        rescompil, resrun = run.run_cpp_code()

        if not resrun:
            resrun = 'No result!'
    else:
        code = default_cpp_code
        resrun = 'No result!'
        rescompil = 'No compilation for C++'
    return render (request,'runcode/main.html', {'code':code,'resrun':resrun,'rescomp':rescompil})
