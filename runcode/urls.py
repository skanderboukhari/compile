from django.urls import path

from . import views

app_name = 'runcode'
urlpatterns = [
    path('',views.run,name='base'),
    path('runpy/',views.runpy,name='runpy'),
    path('runc/',views.runc,name='runc'),
    path('runcpp/',views.runcpp,name='runcpp'),
]
