# set the base img
FROM python:3.8

# create a workdir for img
RUN mkdir usr/src/docker_test

# add project file (current dir ".") to the workdir folder
ADD . /usr/src/docker_test

# set dir where CMD will execute
WORKDIR usr/src/docker_test

# copy file
COPY requirements.txt ./

# get pip to download and install what is in reqs
RUN pip install --no-cache-dir -r requirements.txt

# expose ports 
EXPOSE 8000

# default cmd to execute
CMD exec python3 manage.py runserver 0.0.0.0:8000
