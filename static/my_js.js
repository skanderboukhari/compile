$(document).ready(function() {

    var language = {}

	language['C'] = '#include <stdio.h>\n\nint main(void) \n{\n	printf("Hello World!\\n");\n	return 0;\n}\n';
	language['CPP'] = '#include <iostream>\nusing namespace std;\n\nint main()\n{\n     cout << "Hello World!" << endl;\n     return 0;\n}\n';
	language['CLOJURE'] = '(println "Hello World!")';
	language['CSS'] = "p {\n font-size: 18px; \n}\n";
	language['CSHARP'] = 'using System;\nusing System.Numerics;\nclass Test {\n	static void Main(string[] args)	{\n	   /*\n		* \n		Read input from stdin and provide input before running\n		var line1 = System.Console.ReadLine().Trim();\n		var N = Int32.Parse(line1);\n		for (var i = 0; i < N; i++) {\n		System.Console.WriteLine("hello world");\n		}\n		*/\n\n		System.Console.WriteLine("Hello World!\\n");\n	}\n}\n';
	language['GO'] = 'package main\n\nimport "fmt"\n\nfunc main() {\n    fmt.Println("Hello World")\n}\n';
	language['HASKELL'] = 'module Main\n where\n\nmain=putStrLn "Hello World!\\n"';
	language['JAVA'] = 'class TestClass {\n    public static void main(String args[] ) throws Exception {\n        System.out.println("Hello World!");\n    }\n}\n';
	language['JAVASCRIPT'] = "importPackage(java.io);\nimportPackage(java.lang);\n\nprint ('Hello World!\\n');\n";
	language['LISP'] = '(display "Hello World!")\n';
	language['OBJECTIVEC'] = '#import <Foundation/Foundation.h>\nint main(int argc, const char* argv[]){\n    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];\n    printf("Hello World");\n    [pool drain];\n    return 0;\n}\n';
	language['PASCAL'] = "program Hello;\nbegin\n    writeln ('Hello World!')\nend.\n";
	language['PERL'] = "use strict;\n=comment\n# Read input from stdin and provide input before running code\n# Echo input to output.\nwhile(my $fred = <STDIN>) {\n    print $fred;\n}\n=cut\nprint 'Hello World!'\n";
	language['PHP'] = '<?php\n\necho "Hello World!";\n\n?>\n';
	language['PYTHON'] = "print 'Hello World!'\n";
	language['RUBY'] = "print 'Hello World!'\n";
	language['R'] = 'cat("Hello World")\n';
	language['RUST'] ='fn main() {\n    println!("Hello World!");\n}\n';
	language['SCALA'] = 'object HelloWorld {\n    def main(args: Array[String]) {\n        println("Hello, world!")\n    }\n}\n';
	language['TEXT'] = 'Paste the output here\n';
    <!-- ************  exemple ************ -->
    ace.require("{% static 'language_tools' %}");
    var editor = ace.edit("editor");
    var ongoing = false;
    var selectedLang = "CPP";
    editor.setTheme("ace/theme/tomorrow");
    editor.session.setMode("ace/mode/c_cpp");
    editor.getSession().setTabSize(5);
    var source_code = editor.getValue();
    editor.setFontSize(14);
    editor.setValue(language[selectedLang], -1);
	var StatusBar = ace.require("ace/ext/statusbar").StatusBar;
	var statusBar = new StatusBar(editor, document.getElementById("editor-statusbar"));

	editor.getSession().on('change', function (e) {
        updateContent();

    });

    function updateContent() {
        source_code = editor.getValue();
    }

    //When Changing the language
    $("#lang").change(function(){
		selectedLang = $("#lang").val();
		editor.setValue(language[selectedLang],-1);
		if(selectedLang == "C" || selectedLang == "CPP"){
			var mode = editor.getSession().setMode("ace/mode/c_cpp");
			$('#launch-button').attr('action', "/runcode/run"+ mode);
            $("#launch-button").submit();

		}
		else{
			var mode =  editor.getSession().setMode("ace/mode/" + selectedLang.toLowerCase());
		}
		editor.session.getSelection().clearSelection();

	});
   // document.getElementById('launch-button ').addEventListener("click", function () {
    //    editorElement.innerHTML = editor.getValue();
   // });//To run the code and get all the status


    function runCode(){

		updateContent();


		var token = $(":input[name='csrfmiddlewaretoken']").val();

		var run_data = {
				source: source_code,
				lang: selectedLang,
				csrfmiddlewaretoken:token
		};



	}
    $(".launch-button").click(function(){
			runCode()



	});


});